import flask_restx
from flask import Blueprint


api_blueprint_v1 = Blueprint('google_cv_api' + '_v1', __name__)

api = flask_restx.Api(
    app=api_blueprint_v1,
    version='1.0',
    title='Google CV',
    doc='/docs'
)


def push_environ_to_g():
    #  g.regions_detector = regions_detector
    pass


api_blueprint_v1.before_request(push_environ_to_g)


from . import rest
