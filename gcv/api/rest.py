import json
import uuid

import flask
import flask_restx
from flask import current_app

from vedavaapi.client import VedavaapiSession
from textract.tasks import celery_helper, tasker

from ..celery.worker import segment_task, bulk_segment_concurrent_task
from . import api


# noinspection PyMethodMayBeStatic
@api.route('/')
class RootDocs(flask_restx.Resource):

    def get(self):
        return flask.redirect('docs')


@api.route('/segments')
class Segments(flask_restx.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('page_id', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        gcv_creds_path = current_app.config.get('CREDS_PATH', '/opt/gcv/creds.json')
        credentials_dict = json.loads(open(gcv_creds_path, 'rb').read().decode('utf-8'))

        task_id, reg_info, sresp, eresp = tasker.register_page_ocr_task(
            vs, args['page_id'], celery_helper.get_task_url_fn(api, CeleryTask),
            name='Google OCR on page',
            service='googleocr', work_ext=['googleocr']
        )
        if eresp:
            return eresp

        segment_task.apply_async(
            [vs, reg_info, args['page_id'], credentials_dict],
            task_id=task_id
        )
        return sresp


@api.route('/bulk_segment')
class BulkSegmenter(flask_restx.Resource):

    post_parser = api.parser()
    post_parser.add_argument('site_url', location='form', required=True)
    post_parser.add_argument('access_token', location='form', required=True)
    post_parser.add_argument('book_id', location='form', required=False, default=None)
    post_parser.add_argument('pages_range', location='form', required=False, default=None)
    post_parser.add_argument('page_ids', location='form', required=False, default=None)

    @api.expect(post_parser, validate=True)
    def post(self):
        args = self.post_parser.parse_args()

        pages_range = json.loads(args['pages_range'] or 'null')
        if pages_range is not None and not isinstance(pages_range, list):
            return {"message": "invalid pages_range", "code": 400}, 400

        page_ids = json.loads(args['page_ids'] or 'null')
        if page_ids is not None and type(page_ids) != list:
            return {"message": "invalid page_ids", "code": 400}, 400

        if not args['book_id'] and not page_ids:
            return {"message": "book_id or page_ids have to be given", "code": 400}, 400

        vs = VedavaapiSession(args['site_url'])
        vs.set_access_token(args['access_token'])

        gcv_creds_path = current_app.config.get('CREDS_PATH', '/opt/gcv/creds.json')
        credentials_dict = json.loads(open(gcv_creds_path, 'rb').read().decode('utf-8'))

        task_id, reg_info, sresp, eresp = tasker.register_bulk_ocr_task(
            vs, args['book_id'], celery_helper.get_task_url_fn(api, CeleryTask),
            name='Google OCR on page range',
            service='googleocr', work_ext=['googleocr'], range=pages_range
        )
        if eresp:
            return eresp

        bulk_segment_concurrent_task.apply_async(
            [vs, reg_info, credentials_dict],
            {'book_id': args['book_id'], 'pages_range': pages_range, 'page_ids': page_ids},
            task_id=task_id)

        return sresp


# noinspection PyMethodMayBeStatic
@api.route('/tasks/<task_id>')
class CeleryTask(flask_restx.Resource):

    def get(self, task_id):
        from ..celery import celery_app
        return celery_helper.get_task_status(celery_app, task_id)

    def delete(self, task_id):
        from ..celery import celery_app
        celery_helper.terminate_task(celery_app, task_id)
        return {"task_id": task_id, "success": True}
