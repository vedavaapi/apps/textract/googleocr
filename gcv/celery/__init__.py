from textract.tasks.celery_helper import make_celery

celery_app = make_celery('gcv.celery:celery_app', 'gcv.celery.worker')
