from celery.exceptions import Ignore

from textract.tasks import tasker

from ..lib.segmenter2 import create_segment_resources as _create_segments
from . import celery_app


@celery_app.task(bind=True)
def segment_task(self, vs, reg_info: dict, page_id, credentials_dict):
    task_fn = lambda: _create_segments(
        vs, page_id, credentials_dict, update_state=self.update_state)
    return tasker.wrap_task_in_tasker_flow(vs, reg_info, task_fn)


def bulk_segment(vs, credentials_dict, resolved_page_ids, update_state=None):
    log = {"succeeded": [], "failed": []}
    for i, page_id in enumerate(resolved_page_ids):
        try:
            update_state and update_state(
                state="PROGRESS",
                meta={
                    "status": "segmenting {}".format(page_id),
                    "current": i, "total": len(resolved_page_ids), "page_id": page_id
                }
            )
            _create_segments(vs, page_id, credentials_dict)
            log['succeeded'].append(page_id)
        except Ignore:
            log['failed'].append(page_id)
            pass

    return log['succeeded'], log['failed']


@celery_app.task(bind=True)
def bulk_segment_task(self, vs, reg_info: dict, credentials_dict, book_id=None, pages_range=None, page_ids=None):

    bulk_task_fn = lambda resolved_page_ids: bulk_segment(
        vs, credentials_dict, resolved_page_ids, update_state=self.update_state)

    return tasker.wrap_bulk_task_in_tasker_flow(
        vs, reg_info, bulk_task_fn,
        book_id=book_id, pages_range=pages_range, page_ids=page_ids,
        update_state=self.update_state
    )


def bulk_segment_concurrent(vs, credentials_dict, resolved_page_ids, update_state=None):
    log = {"succeeded": [], "failed": []}
    try:
        task_id = update_state.__self__.request.id if update_state else None
    except:
        task_id = None

    def broadcast_state():
        if not update_state:
            return
        update_state(
            task_id=task_id, state='PROGRESS',
            meta={
                "total": len(resolved_page_ids),
                "current": len(log['succeeded']) + len(log['failed']),
                'status': '{} pages succeeded'.format(len(log['succeeded'])),
            }
        )

    def segmenter_fn(page_id):
        try:
            result = _create_segments(vs, page_id, credentials_dict)
            log['succeeded'].append(page_id)
        except Ignore:
            result = None
            log['failed'].append(page_id)
        broadcast_state()
        return result

    import concurrent.futures
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(segmenter_fn, resolved_page_ids)

    return log['succeeded'], log['failed']


@celery_app.task(bind=True)
def bulk_segment_concurrent_task(
        self, vs, reg_info: dict, credentials_dict, book_id=None, pages_range=None, page_ids=None):

    bulk_task_fn = lambda resolved_page_ids: bulk_segment_concurrent(
        vs, credentials_dict, resolved_page_ids, update_state=self.update_state)

    return tasker.wrap_bulk_task_in_tasker_flow(
        vs, reg_info, bulk_task_fn,
        book_id=book_id, pages_range=pages_range, page_ids=page_ids,
        update_state=self.update_state
    )
