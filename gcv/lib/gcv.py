import base64
import json
import time
from functools import reduce
import requests

from google.oauth2 import service_account

from textract.books.segmenters.hocr_models import Polygon, HOCRPage, HOCRCArea, HOCRParagraph, HOCRWord, TextAnnotation


class GCVClient(object):

    def __init__(self, creds: service_account.Credentials):
        self.creds = creds
        self._annotator = None

    @classmethod
    def from_creds_json(cls, creds):
        return cls(service_account.Credentials.from_service_account_info(creds))

    @property
    def annotator(self):
        if not self._annotator:
            kwargs = {"credentials": self.creds}
            from google.cloud import vision_v1 as vision
            self._annotator = vision.ImageAnnotatorClient(**kwargs)
        return self._annotator

    def detect(self, image_uri=None, image_content=None):
        from google.cloud import vision_v1 as vision  # vision.types error on other envs.
        if image_uri:
            # noinspection PyUnresolvedReferences
            image = vision.types.Image()
            image.source.image_uri = image_uri
        elif image_content:
            # noinspection PyUnresolvedReferences
            image = vision.types.Image(content=image_content)
        else:
            return None
        response = self.annotator.document_text_detection(image=image)
        #  print(response.full_text_annotation)
        return response.full_text_annotation


class GCVClientRest(object):

    def __init__(self, creds_dict:dict=None):
        self.creds = None
        self.creds_dict = creds_dict or {}
        self.signed_jwt = None
        self.exp = None

    def _get_jwt(self):
        if self.signed_jwt and time.time() + 300 < self.exp:  # 5 minuits threshold
            return self.signed_jwt  # TODO expiry?
        # @see https://developers.google.com/identity/protocols/oauth2/service-account#python
        iat = time.time()
        exp = iat + 3600
        payload = {
            'iss': self.creds_dict['client_email'],
            'sub': self.creds_dict['client_email'],
            'aud': 'https://vision.googleapis.com/',
            'iat': iat,
            'exp': exp
        }
        additional_headers = {'kid': self.creds_dict['private_key_id']}
        import jwt
        self.signed_jwt = jwt.encode(
            payload, self.creds_dict['private_key'],
            headers=additional_headers, algorithm='RS256').decode('utf-8')
        self.exp = exp
        return self.signed_jwt

    @classmethod
    def from_creds_json(cls, creds_json):
        return cls(creds_json)

    def detect(self, image_uri=None, image_content=None):
        image = {}
        if image_uri:
            image['source'] = {'imageUri': image_uri}
        elif image_content:
            image['content'] = base64.b64encode(image_content)

        _requests = [{"image": image, "features": [{"type": "DOCUMENT_TEXT_DETECTION"}]}]
        headers = {"Authorization": f'Bearer {self._get_jwt()}'}
        print(headers)
        resp = requests.post(
            'https://vision.googleapis.com/v1/images:annotate',
            json={"requests": _requests}, headers=headers)
        open('/home/damodarreddy/gcv_resp.json', 'wb').write(json.dumps(resp.json()).encode('utf-8'))
        resp.raise_for_status()
        return None


# image regions will be atttched as children to page itself, not part of layout hierarchy. in hierarchy, they will be inclded as members
def gcv_word_to_hocr(word, container_id, index, page_id):
    # returns ImageRegion(HOCRWord)
    hocr_word = HOCRWord(
        '_:{}-w{}'.format(container_id, index),
        region=Polygon(word.bounding_box.vertices),
        parent=page_id
    )  # region

    #  print(vars(word.symbols[-1].property.detected_break))
    word_detected_break_type = word.symbols[-1].property.detected_break.type_
    word_seperator = ['', ' ', ' ', '\n', '-', '\n'][word_detected_break_type]
    word_text = reduce(lambda w, s: w + s.text, word.symbols, '') + word_seperator

    symbols_confidence = [s.confidence or 1 for s in word.symbols]
    # noinspection PyProtectedMember
    word_anno = TextAnnotation(
        '_:{}-ta'.format(hocr_word._id),
        word_text,
        parent=hocr_word._id,
        confidence=word.confidence or 1,
        generator='GOOGLE CLOUD VISION',
        #  member_confidence=symbols_confidence,
    )
    hocr_word.annos.append(word_anno)
    return hocr_word


"""
NOTE! that HOCR line has no parallel in GCV annotation.
we can simulate if we want for uniformity sake, or just save what we got.
clients should not assume existing of al hocr levels in layout.
"""


def gcv_paragraph_to_hocr(paragraph, parent_id, index, page_id):
    hocr_paragraph = HOCRParagraph(
        '_:{}-prg{}'.format(parent_id, index),
        region=Polygon(paragraph.bounding_box.vertices),
        parent=parent_id, index=index
    )
    # noinspection PyProtectedMember
    hocr_paragraph.members = [
        gcv_word_to_hocr(word, hocr_paragraph._id, i, page_id)
        for i, word in enumerate(paragraph.words)
    ]
    return hocr_paragraph


def gcv_block_to_hocr(block, parent_id, index, page_id):
    hocr_carea = HOCRCArea(
        '_:{}-c{}'.format(parent_id, index),
        region=Polygon(block.bounding_box.vertices),
        parent=parent_id, index=index
    )
    # noinspection PyProtectedMember
    hocr_carea.children = [
        gcv_paragraph_to_hocr(paragraph, hocr_carea._id, i, page_id)
        for i, paragraph in enumerate(block.paragraphs)
    ]
    return hocr_carea


def gcv_page_to_hocr(page, page_id, index):
    hocr_page = HOCRPage(
        '_:{}-p{}'.format(page_id, index),
        region=Polygon.from_rectangle(0, 0, page.width, page.height),
        parent=page_id
    )
    # noinspection PyProtectedMember
    hocr_page.children = [
        gcv_block_to_hocr(block, hocr_page._id, i, page_id)
        for i, block in enumerate(page.blocks)
    ]
    return hocr_page


def gcv_anno_to_hocr_pages(gcv_anno, page_id):
    return [
        gcv_page_to_hocr(page, page_id, i)
        for i, page in enumerate(gcv_anno.pages)
    ]
