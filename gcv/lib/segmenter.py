import json
import re
import sys
from collections import namedtuple
from functools import reduce

from celery.exceptions import Ignore
from requests import HTTPError

from textract_segmenters_common import books_helper, svg_helper

Vertex = namedtuple('Vertex', ['x', 'y'])


class DetectedRegion(object):

    def __init__(self, vertices, region_cls=None, score=None):
        self.vertices = [Vertex(v.x, v.y) for v in vertices]
        self.region_cls = region_cls
        self.score = score

    def polygon_svg(self):
        return svg_helper.svg_for_polygon(self.vertices)

    @classmethod
    def get_bounding_box(cls, vertices):
        min_x = vertices[0].x
        min_y = vertices[0].y
        max_x = vertices[0].x
        max_y = vertices[0].y
        for v in vertices[1:]:
            if v.x < min_x:
                min_x = v.x
            if v.y < min_y:
                min_y = v.y
            if v.x > max_x:
                max_x = v.x
            if v.y > max_y:
                max_y = v.y

        return min_x, min_y, max_x - min_x, max_y - min_y

    def get_bounding_rectangle_xywh(self):
        return self.get_bounding_box(self.vertices)


def fragment_selector_from_bounding_poly(vertices):
    region = DetectedRegion(vertices)
    rectangle = {"jsonClass": "WrapperObject"}
    xywh = region.get_bounding_rectangle_xywh()

    rectangle.update({
        "x": int(xywh[0]),
        "y": int(xywh[1]),
        "w": int(xywh[2]),
        "h": int(xywh[3])
    })

    fragment_selector = {
        "jsonClass": "FragmentSelector",
        "value": "xywh={},{},{},{}".format(rectangle['x'], rectangle['y'], rectangle['w'], rectangle['h']),
        "rectangle": rectangle
    }
    return fragment_selector


def svg_selector_from_bounding_poly(vertices):
    region = DetectedRegion(vertices)
    svg_selector = {
        "jsonClass": "SvgSelector",
        "value": region.polygon_svg()
    }
    return svg_selector


def get_section_res(source, index, vertices, section_type):
    selector = {
        "jsonClass": "SvgFragmentSelectorChoice",
        "default": fragment_selector_from_bounding_poly(vertices),
        "item": svg_selector_from_bounding_poly(vertices)
    }

    section = {
        "jsonClass": "Resource",
        "source": source,
        "selector": selector,
        "state": "system_inferred",
        "jsonClassLabel": section_type,
    }

    if index is not None:
        section['index'] = index

    return section


def get_members_selector(resource_ids=None):
    members_selector = {
        # "jsonClass": "MembersSelector"
    }
    if isinstance(resource_ids, list):
        members_selector['resource_ids'] = resource_ids
    return members_selector


def get_gcv_anno_client(creds_json):
    # noinspection PyProtectedMember
    from google.auth import _service_account_info
    signer = _service_account_info.from_dict(creds_json)

    from google.oauth2 import service_account
    # noinspection PyProtectedMember
    service_creds = service_account.Credentials._from_signer_and_info(signer, creds_json.copy())

    kwargs = {"credentials": service_creds}
    from google.cloud import vision
    return vision.ImageAnnotatorClient(**kwargs)


def segment(vc, oold_id, credentials_dict):
    cv_client = get_gcv_anno_client(credentials_dict)

    oold_url = books_helper.get_oold_url(vc, oold_id)
    local_url_re = re.compile(r'^http://localhost[:/]|http://[0-9\\.]*[:/]')

    from google.cloud import vision
    #  if local_url_re.match(oold_url):
    file_content = books_helper.get_oold(vc, oold_id)
    # noinspection PyUnresolvedReferences
    image = vision.types.Image(content=file_content)
    """
    else:
        # noinspection PyUnresolvedReferences
        image = vision.types.Image()
        image.source.image_uri = oold_url
        print(oold_url)
    """

    print("performing text detection")
    response = cv_client.document_text_detection(image=image)
    print("text detection completed")
    return response.full_text_annotation


def marshal_to_vv(page_id, gcv_tree):
    layout = {}
    regions = {}

    class Line(object):
        def __init__(self):
            self.words = []

        @property
        def vertices(self):
            vs = []
            for _word in self.words:
                vs.extend(_word.bounding_box.vertices)
            return vs

    for i, page in enumerate(gcv_tree.pages):
        page_blank_id = '_:page-{}'.format(i)
        vertices = [Vertex(0, 0), Vertex(page.width, 0), Vertex(page.width, page.height), Vertex(0, page.height)]
        page_section = get_section_res(page_id, i, vertices, 'HOCRPage')
        layout[page_blank_id] = page_section

        wc = 0
        #  open('/home/damodarreddy/gcvout.txt', 'wb').write(str(page).encode('utf-8'))
        for j, block in enumerate(page.blocks):
            block_blank_id = '_:block-{}-{}'.format(i, j)
            block_section = get_section_res(page_blank_id, j, block.bounding_box.vertices, 'HOCRCArea')
            layout[block_blank_id] = block_section

            for k, pg in enumerate(block.paragraphs):
                pg_blank_id = '_:pg-{}-{}-{}'.format(i, j, k)
                pg_section = get_section_res(block_blank_id, k, pg.bounding_box.vertices, 'HOCRParagraph')
                layout[pg_blank_id] = pg_section

                lines = []
                cur_line = Line()
                lines.append(cur_line)
                for l, word in enumerate(pg.words):
                    #  print(reduce(lambda w, s: w + s.text, word.symbols, ''), word.symbols[-1].property.detected_break.type)
                    cur_line.words.append(word)
                    if word.symbols[-1].property.detected_break.type in (3, 5):
                        cur_line = Line()
                        lines.append(cur_line)

                for l, line in enumerate(lines):
                    line_vertices = line.vertices
                    if not len(line.vertices):
                        print('empty ine')
                        continue
                    line_blank_id = '_:line-{}-{}-{}-{}'.format(i, j, k, l)
                    bb = DetectedRegion.get_bounding_box(line.vertices)
                    line_vertices = [
                        Vertex(bb[0], bb[1]), Vertex(bb[0] + bb[2], bb[1]),
                        Vertex(bb[0] + bb[2], bb[1] + bb[3]), Vertex(bb[0], bb[1] + bb[3])
                    ]
                    line_section = get_section_res(pg_blank_id, l, line_vertices, 'HOCRLine')
                    layout[line_blank_id] = line_section

                    member_word_blank_ids = []
                    for m, word in enumerate(line.words):
                        word_blank_id = '_:word-{}-{}-{}-{}-{}'.format(i, j, k, l, m)
                        word_section = get_section_res(page_id, wc + 1, word.bounding_box.vertices, 'HOCRWord')
                        word_section['jsonClass'] = 'ImageRegion'
                        regions[word_blank_id] = word_section
                        member_word_blank_ids.append(word_blank_id); wc += 1

                        word_text = reduce(lambda w, s: w + s.text, word.symbols, '')
                        #  print(word_text)
                        word_anno_blank_id = '{}-anno'.format(word_blank_id)
                        word_detected_break_type = word.symbols[-1].property.detected_break.type
                        word_seperator = ['', ' ', ' ', '\n', '-', '\n'][word_detected_break_type]

                        # print(word_text, word_detected_break_type, word_seperator)

                        word_anno = {
                            "_id": word_anno_blank_id,
                            "jsonClass": "TextAnnotation",
                            "target": word_blank_id,
                            "body": [{
                                "jsonClass": "Text",
                                "chars": word_text + word_seperator
                            }]
                        }
                        regions[word_anno_blank_id] = word_anno
                    line_section['members'] = get_members_selector(resource_ids=member_word_blank_ids)

    return layout, regions


def set_resolved_region_ids(graph, region_id_uid_graph):
    for item in graph.values():
        if not item.get('members', {}).get('resource_ids', []):
            continue
        resource_ids = item['members']['resource_ids']
        for i, rid in enumerate(resource_ids):
            resource_ids[i] = region_id_uid_graph[rid]


def post_hierarchy_and_regions(vs, hierarchy, regions_graph, update_state=None):
    graph_post_url = 'objstore/v1/graph'
    regions_graph_post_data = {
        "graph": json.dumps(regions_graph),
        "should_return_resources": "false",
        "should_return_oold_resources": "false"
    }
    try:
        print("posting regions_graph", file=sys.stderr)
        regions_post_response = vs.post(graph_post_url, data=regions_graph_post_data)
        regions_post_response.raise_for_status()
    except HTTPError as e:
        if update_state:
            update_state(
                state='Failure',
                meta={
                    "status": "error in posting regions_graph: {}".format(e.response.json().get('error', '')),
                    "error": str(e),
                    "error_code": e.response.status_code
                }
            )
        raise Ignore()

    set_resolved_region_ids(hierarchy, regions_post_response.json()['graph'])
    hierarchy_graph_post_data = {
        "graph": json.dumps(hierarchy),
        "should_return_resources": "false",
        "should_return_oold_resources": "false"
    }

    try:
        print("posting hierarchy", file=sys.stderr)
        hierarchy_post_response = vs.post(graph_post_url, data=hierarchy_graph_post_data)
        hierarchy_post_response.raise_for_status()
    except HTTPError as e:
        if update_state:
            update_state(
                state='Failure',
                meta={
                    "message": "error in posting regions_graph",
                    "error": str(e),
                    "error_code": e.response.status_code
                }
            )
        raise Ignore()

    return regions_post_response.json(), hierarchy_post_response.json()


def create_segment_resources(vs, page_id, credentials_dict, update_state=None):

    update_state = update_state or (lambda *args, **kwargs: None)

    update_state(state='PROGRESS', meta={"status": "deleting existing regions", "current": 0, "total": 5})
    books_helper.delete_previous_regions(vs, page_id, update_state=update_state)

    update_state(state='PROGRESS', meta={"status": "retrieving page description", "count": 1, "total": 5})
    page_image_oold_id = books_helper.get_page_image_id(vs, page_id, update_state=update_state)

    update_state(state='PROGRESS', meta={"status": "performing google ocr", "current": 2, "total": 5})
    gcv_tree = segment(vs, page_image_oold_id, credentials_dict)

    update_state(state='PROGRESS', meta={"status": "marshalling google ocr result", "current": 3, "total": 5})
    hierarchy, regions = marshal_to_vv(page_id, gcv_tree)

    update_state(state='PROGRESS', meta={"status": "saving ocr data to vedavaapi site.", "current": 4, "total": 5})
    response = post_hierarchy_and_regions(vs, hierarchy, regions, update_state=update_state)

    return {"status": "ocr completed", "current": 5, "total": 5}
