from textract import books
from textract.models import ScannedPage
from textract.books.segmenters.hocr_models import persist_hocr_pages
from textract.tasks.celery_helper import default_update_state

from ..lib.gcv import GCVClient, gcv_anno_to_hocr_pages


URL_REF_PROBLEMATIC_NAMESPACES = ['archive.org']


def segment(scanned_page: ScannedPage, gcv_client: GCVClient):
    """
    for now we will download content and pass it on to gcv.
    we will once come to policy conclusions, on to when can we send just url ref instead.
    """
    oold_namespace = scanned_page.image_oold.json['namespace']
    resolved_image_url = (
        scanned_page.image_url if (oold_namespace in ['_vedavaapi', '_abstract'])
        else scanned_page.image_oold.json['url']
    )
    '''
    import re
    from urllib.parse import urlparse
    parsed_url = urlparse(resolved_image_url)
    is_registered_hostname = (
            parsed_url.hostname != 'localhost' and  # not localhost
            not re.fullmatch(r'[0-9][0-9.]+', parsed_url.hostname)  # not ip address
    )
    can_refer_through_url = (
            is_registered_hostname and
            oold_namespace not in URL_REF_PROBLEMATIC_NAMESPACES
    )
    '''
    # print({'can_refer_through_url': can_refer_through_url, "resolved_url": resolved_image_url})
    can_refer_through_url = False
    return (
        gcv_client.detect(image_uri=resolved_image_url) if can_refer_through_url
        else gcv_client.detect(image_content=scanned_page.image_content)
    )


def create_segment_resources(vs, page_id, credentials_dict, update_state=None):
    update_state = update_state or default_update_state
    #  update_state = lambda *args, **kwargs: print(args, kwargs) or _update_state(*args, **kwargs)

    update_state(state='PROGRESS', meta={"status": "deleting existing regions", "current": 0, "total": 5})
    books.delete_existing_regions(vs, page_id, update_state=update_state, resources_state='system_inferred')

    update_state(state='PROGRESS', meta={"status": "retrieving page description", "count": 1, "total": 5})
    scanned_page = ScannedPage.load_with_oold_def(vs, page_id, update_state=update_state)
    #  print(scanned_page.json)

    # we may can directly pass gcv_client?
    gcv_client = GCVClient.from_creds_json(credentials_dict)

    update_state(state='PROGRESS', meta={"status": "performing segmentation", "current": 2, "total": 5})
    # any error thrown here? no update_state passed
    gcv_anno = segment(scanned_page, gcv_client)
    #  print('gcv_anno', gcv_anno)

    update_state(state='PROGRESS', meta={"status": "marshalling gcv resp to vedavaapi objects", "current": 3, "total": 5})
    # noinspection PyProtectedMember
    hocr_pages = gcv_anno_to_hocr_pages(gcv_anno, scanned_page._id)

    update_state(state='PROGRESS', meta={"status": "persisting objects to vedavaapi objstore", "current": 4, "total": 5})
    # noinspection PyUnusedLocal
    regions_graph, layout_graph = persist_hocr_pages(vs, hocr_pages, update_state=update_state)

    return dict(status="detection success", current=5, total=5)
